//This SMS package allows you to send and receive SMS msgs from a number of gateway providers
//To receive msgs point the SMS gateway webhook to: /api/sms/<provider>/receive


//Requires a table with the following schema
//#region SQL Table
/*
DROP TABLE [dbo].[SMSLogs]
GO
CREATE TABLE [dbo].[SMSLogs] (
[id] int NOT NULL ,
[Sent] tinyint NOT NULL DEFAULT ((0)) ,
[Msg] varchar(512) NULL ,
[Response] text NULL ,
[Timestamp] datetime NULL DEFAULT (getdate()) ,
[CustomText] text NULL
)
-- ----------------------------
-- Primary Key structure for table [dbo].[SMSLogs]
-- ----------------------------
ALTER TABLE [dbo].[SMSLogs] ADD PRIMARY KEY ([id])
GO
*/
//#endregion


//Customise this function to whatever you like
//It should call _sendSMS(provider,params);
sendSMS = function(number, msg, userType, userId){

  if(!msg || !number)
    return;

  let HCPId = (userType == "HCP") ? userId : null;
  let PatientId = (userType == "Patients") ? userId : null;

  //Ensure user has sms_enabled
  let q = Sql.q(`SELECT sms_enabled FROM `+userType+` WHERE mobile = @number`,{number});
  if(!q || q.length == 0 || q[0].sms_enabled == 0 )
  {
    console.log("User with mobile: "+number+" not found or trying to send SMS to user who has sms disabled: "+userType+" id:"+userId);
    return;
  }

  _sendSMS("clicksend",{
    custom_json   : {
      HCPId       : HCPId,
      PatientId   : PatientId
    },
    from          : SMS_SENDER_ID,
    to            : number,
    msg           : msg,
    username      : SMS_USERNAME,
    api_key       : SMS_PASSWORD
  });
}

//Will send an SMS using the given provider
//provider: "clicksend"
function _sendSMS(provider, params){
  var response = null;
  switch(provider)
  {
    case "clicksend":
      response = clicksend_send_SMS(params);
    break;
  }
  Sql.q(`INSERT INTO SMSLogs (Sent, Msg, Response, Error, Timestamp, CustomText)
          VALUES(1, @msg, @response, @error, GetDate(), @custom_text )`,
        {
          msg : params.msg,
          response: (response.result) ? JSON.stringify(response.result) : null,
          error : (response.error) ? JSON.stringify(response.error) : null,
          custom_text: JSON.stringify(params.custom_json)
        }
  );
  return response;
}


/////////////// RECEIVE CALLBACKS /////////////
//Types of texts that can be received
//YES: Send, please tell us date of last injection SMS
//NO:
//DD/MM/YYYY: Update the patietns last injection
clicksend_callback = function(response){

  Sql.q(`INSERT INTO SMSLogs (Sent, Msg, Response, Error, Timestamp, CustomText)
          VALUES(0, @msg, @response, @error, GetDate(), @custom_text )`,
        {
          msg : (response.result) ? response.result.message : null,
          response: (response.result) ? JSON.stringify(response.result) : null,
          error : (response.error) ? JSON.stringify(response.error) : null,
          custom_text: null
        });

  if(response.error)
    return;

  let from_mobile = response.result.sms;
  let msg = response.result.message;

  //Get patient from mobile number
  let valid_mobiles = [
                    "'"+from_mobile+"'",  //+61408263865
                    "'"+from_mobile.replace("+61","0")+"'", //0408263865
                    "'"+from_mobile.replace("+","")+"'" //61408263865
  ].join(",");

  //Find Patient
  let queryText = `SELECT TOP 1 id, first_name, mobile FROM Patients WHERE mobile IN ( `+valid_mobiles+` );`;
  var aPatient = Sql.q(queryText);
  var aSupporter = null;
  if(!aPatient || aPatient.length == 0){
    queryText = `SELECT TOP 1 p.id, p.first_name, s.first_name AS supporter_first_name, s.id AS supporter_id, s.mobile AS supporter_mobile
    FROM Patients p
    JOIN Supporters s ON s.id = p.supporter_id
    WHERE p.mobile IN (`+valid_mobiles+`)`;
    aSupporter = Sql.q(queryText);
  }

  if(!aPatient && !aSupporter || (aPatient.length == 0 && aSupporter.length == 0))
  {
    console.log("Could not find Patient or Supporter with mobile #s: "+valid_mobiles);
    return;
  }
  if(aPatient)
    aPatient = aPatient[0];
  if(aSupporter)
    aSupporter = aSupporter[0];

  let number    = (aPatient) ? aPatient.mobile : aSupporter.supporter_mobile;
  let userType  = (aPatient) ? "Patients" : "HCP";
  let userId    = (aPatient) ? aPatient.id : aSupporter.supporter_id;
  let reply_msg = "";
  switch(msg.toLowerCase())
  {
    //Disable SMS for Patient or Supporter
    case "stop":
      if(aPatient)
        Sql.q("UPDATE Patients SET sms_enabled = 0 WHERE id = @id",{id: aPatient.id});
      else if (aSupporter)
        Sql.q("UPDATE Supporters SET sms_enabled = 0 WHERE id = @id",{id: aSupporter.supporter_id});
      return; //Dont send SMS
    break;


    //Request Patient or Supporter to send us the date
    case "yes":
      reply_msg = getSMSText((aPatient) ? "sms_yes_reply" : "sms_s_yes_reply",
                            (aPatient) ? [{patient_first_name : aPatient.first_name}] : [{support_first_name : aSupporter.supporter_first_name, patient_first_name : aSupporter.first_name}]);
    break;

    case "no":
      //TODO:
    break;

    //Check if msg converts to date
    default:
      reply_msg = getSMSText((aPatient) ? "sms_error_reply" : "sms_s_error_reply",
                            (aPatient) ? [{patient_first_name : aPatient.first_name}] : [{support_first_name : aSupporter.supporter_first_name, patient_first_name : aSupporter.first_name}]);

      try{
        //Replace - for /
        msg = msg.replace(/-/g,"/");
        if(moment(msg, 'DD/MM/YYYY',true).isValid()){

          //INSERT new injection
          Sql.q("INSERT INTO Injections (patient_id,date) VALUES (@patient_id, @injection_date)",
              {
                patient_id: aPatient.id,
                injection_date : msg
              });

          reply_msg = "Thank you! Injection date has been updated.";
        }
      }
      catch(error){
        console.log(error);
      }
    break;
  }
  if(reply_msg != "")
    sendSMS(number, reply_msg, userType, userId);
}





