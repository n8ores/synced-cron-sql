import "./crons_sql.js";

SyncedCron.config({log: false});


//Will email any patients who have not logged into my prolia after x days
SyncedCron.add({
name: 'CRON: Check logins',
schedule: function(parser) {
    // parser is a later.parse object
    return parser.text('every 1 day');
},
job: function() {

    //Check for patients that haven't logged in within 5 days of enrolment
    let q = Sql.q(`SELECT first_name, last_name, email FROM Patients
                    WHERE DATEDIFF(DAY,enrolment_date,CURRENT_TIMESTAMP) = 5 AND
                    (id NOT IN (SELECT patient_id AS id FROM Activity WHERE DATEDIFF(DAY,login_timestamp,CURRENT_TIMESTAMP) <= 5))`);
    if(q && q.length>0)
    {
        for(var i=0; i<q.length; i++)
        {
            let aPatient = q[i];
            let emailParams = [{patient_first_name : aPatient.first_name}];
            let emailObj = getEmail("email_no_login_five",emailParams);
            emailObj.to = aPatient.email;
            sendEmail(emailObj);
        }
    }

    //Check for patients that haven't logged in within 65 days of enrolment
    q = Sql.q(`SELECT first_name, last_name, email FROM Patients
                    WHERE DATEDIFF(DAY,enrolment_date,CURRENT_TIMESTAMP) = 65 AND
                    (id NOT IN (SELECT patient_id AS id FROM Activity WHERE DATEDIFF(DAY,login_timestamp,CURRENT_TIMESTAMP) <= 65))`);
    if(q && q.length>0)
    {
        for(var i=0; i<q.length; i++)
        {
            let aPatient = q[i];
            let emailParams = [{patient_first_name : aPatient.first_name}];
            let emailObj = getEmail("email_no_login_sixty_five",emailParams);
            emailObj.to = aPatient.email;
            sendEmail(emailObj);
        }
    }

    return "success";
}
});


SyncedCron.add({
    name: 'CRON: Send Alerts',
    schedule: function(parser) {
        // parser is a later.parse object
        return parser.text('every 30 seconds');
    },
    job: function() {
        sendAlerts();
        return "success";
    }
});

SyncedCron.add({
    name: 'CRON: Send DoctorEnrolment',
    schedule: function(parser) {
        // parser is a later.parse object
        return parser.text('every 24 hour');
    },
    job: function() {

        //Get all HCP Professionals that havent been sent their welcome email.
        let q = Sql.q("SELECT id, email, onekey_dr_id FROM HCP WHERE DATEDIFF(DAY,enrolment_date, GETDATE()) = 5 AND enrolment_email_sent = 0");
        if(q && q.length>0)
        {
            let q2 = Sql.q('SELECT FIRST_NAME, LAST_NAME FROM Doctors WHERE "ONEKEY INDIVIDUALID" = @onekey_dr_id',{onekey_dr_id : q[0].onekey_dr_id });
            if(q2 && q2.length > 0)
            {
                //SEND HCP WELCOME
                let emailParams = [{dr_first_name :q2[0].FIRST_NAME}];
                let emailObj = getEmail("email_hcp_enrolment",emailParams);
                emailObj.to = q[0].email;
                sendEmail(emailObj);

                //Update the flag
                Sql.q("UPDATE HCP SET enrolment_email_sent = 1 WHERE id = @id",{id : q[0].id});
            }

        }
        return "success";
    }
});



SyncedCron.add({
    name: 'CRON: Import data',
    schedule: function(parser) {
        // parser is a later.parse object
        return parser.text('every 24 hour');
    },
    job: function() {
        //See testmethod
        //Get new file from FTP
        return "success";
    }
});



//Get patients who'se last injection
sendAlerts = function(){
    console.log("Running Alerts CRON");
    //Query returns all the latest injections for each patient, and joins the Patients and Supporters table
    //To return the required data
    let q = Sql.q(`SELECT
                Injections.id, Injections.date, DATEDIFF(DAY,date,CURRENT_TIMESTAMP) AS days_since_last, DATEADD(DAY,`+SIX_MONTHS_DAYS+`,date) AS next_injection_date, Injections.patient_id,
                Injections.edu_one_sent, Injections.edu_two_sent, Injections.edu_three_sent, Injections.edu_four_sent, Injections.edu_five_sent, Injections.edu_six_sent,
                Injections.is_first_injection, Injections.on_day_alert_sent, Injections.six_one_alert_sent, Injections.six_two_alert_sent,
                Injections.seven_two_alert_sent, Injections.eleven_two_alert_sent,
                Injections.five_alert_sent, Injections.five_two_alert_sent,Injections.day_before_alert_sent,
                Patients.first_name, Patients.last_name, Patients.email, Patients.mobile, Patients.supporter_id,
                Patients.sms_enabled, Patients.emails_enabled,
                Supporters.first_name AS s_first_name, Supporters.last_name AS s_last_name, Supporters.relationship As s_relationship, Supporters.email AS s_email, Supporters.mobile AS s_mobile, Supporters.sms_enabled AS s_sms_enabled, Supporters.emails_enabled AS s_emails_enabled
            FROM
            (SELECT
                patient_id, MAX(id) AS last_injection_id
            FROM Injections
            GROUP BY patient_id) AS latest_injections
            INNER JOIN
            Injections
            ON
            Injections.patient_id = latest_injections.patient_id AND
            Injections.id = latest_injections.last_injection_id
            INNER JOIN Patients ON Patients.id = Injections.patient_id
            LEFT JOIN Supporters ON Supporters.id = Patients.supporter_id
            `,{});

    if(!q || q.length ==0)
        return;

    var listOfAlerts = [];
    for(var i=0; i<q.length; i++)
    {
        let aRecord = q[i];
        if(aRecord.sms_enabled == 0) continue;
        let to_email = (aRecord.s_email) ? aRecord.s_email : aRecord.email

        //Injection overdue by 1 week
        let aAlert = {
            injection_id: aRecord.id,
            mobile      : aRecord.mobile,
            s_mobile    : aRecord.s_mobile,
            email       : aRecord.email,
            s_email     : aRecord.s_email,
            userType    : "Patients",
            userId      : aRecord.patient_id
        };

        //Generate some data used by some emails
        let setVerificationToken = false;
        let verification_token = generateRandomString(50, false);
        let update_injection_url = "<a href='"+Meteor.absoluteUrl()+"update_injection_date/"+verification_token+"/patient'>Update last injection</a>";
        let s_update_injection_url= "<a href='"+Meteor.absoluteUrl()+"update_injection_date/"+verification_token+"/supporter'>Update last injection</a>";

        //////////////// EDUCATION ///////////////////
        if(aRecord.days_since_last == EDU_ONE_DAYS && aRecord.edu_one_sent == 0)
        {
            let id = "edu_one";
            aAlert.flag        = id+"_sent";
            aAlert.emailObj    = getEmail("email_"+id,[{patient_first_name : aRecord.first_name}]);
        }
        else if (aRecord.days_since_last == EDU_TWO_DAYS && aRecord.edu_two_sent == 0)
        {
            let id = "edu_two";
            aAlert.flag        = id+"_sent";
            aAlert.emailObj    = getEmail("email_"+id,[{patient_first_name : aRecord.first_name}]);
        }
        else if (aRecord.days_since_last == EDU_THREE_DAYS && aRecord.edu_three_sent == 0)
        {
            let id = "edu_three";
            aAlert.flag        = id+"_sent";
            aAlert.emailObj    = getEmail("email_"+id,[{patient_first_name : aRecord.first_name}]);
        }
        else if (aRecord.days_since_last == EDU_FOUR_DAYS && aRecord.edu_four_sent == 0)
        {
            let id = "edu_four";
            aAlert.flag        = id+"_sent";
            aAlert.emailObj    = getEmail("email_"+id,[{patient_first_name : aRecord.first_name}]);
        }
        else if (aRecord.days_since_last == EDU_FIVE_DAYS && aRecord.edu_five_sent == 0)
        {
            let id = "edu_five";
            aAlert.flag        = id+"_sent";
            aAlert.emailObj    = getEmail("email_"+id,[{patient_first_name : aRecord.first_name}]);
        }
        else if (aRecord.days_since_last == EDU_SIX_DAYS && aRecord.edu_six_sent == 0)
        {
            let id = "edu_six";
            aAlert.flag        = id+"_sent";
            aAlert.emailObj    = getEmail("email_"+id,[{patient_first_name : aRecord.first_name}]);
        }


        //////////////// REMINDERS ///////////////////

        //ONE MONTH PRIOR (5 months since last injection) - EMAIL ONLY
        if(aRecord.days_since_last == ONE_MONTH_PRIOR && aRecord.five_alert_sent == 0)
        {
            let id = "five";
            aAlert.flag        = id+"_alert_sent";
            aAlert.emailObj    = getEmail("email_"+id,[{patient_first_name : aRecord.first_name}, {insert_date : moment(aRecord.next_injection_date).format("D MMMM YYYY")}]);
            aAlert.s_emailObj  = getEmail("email_support_"+id,[{support_first_name: aRecord.s_first_name}, {patient_first_name: aRecord.first_name},{insert_date : moment(aRecord.next_injection_date).format("D MMMM YYYY")}]);
        }
        //TWO WEEKS PRIOR (5.5 Months since last injection)
        else if( aRecord.days_since_last == TWO_WEEKS_PRIOR && aRecord.five_two_alert_sent == 0)
        {
            let id = "five_two";
            aAlert.flag       = id+"_alert_sent";
            aAlert.smsText    = getSMSText("sms_"+id,[{patient_first_name : aRecord.first_name}, {insert_date : moment(aRecord.next_injection_date).format("D MMMM YYYY")}]);
            aAlert.s_smsText  = aRecord.s_mobile ? getSMSText("sms_s_"+id,[{support_first_name : aRecord.s_first_name}, {insert_date : moment(aRecord.next_injection_date).format("D MMMM YYYY")}, {patient_first_name: aRecord.first_name}]) : null;
        }
        //Injection due TOMORROW (6 months - 1 day since last injection) - EMAIL ONLY
        else if (aRecord.days_since_last == SIX_MONTHS_DAYS - 1 && aRecord.day_before_alert_sent == 0)
        {
            let id = "day_before";
            aAlert.flag       = id+"_alert_sent";
            aAlert.emailObj   = getEmail("email_"+id,[{patient_first_name : aRecord.first_name}]);
            aAlert.s_emailObj = getEmail("email_support_"+id,[{support_first_name: aRecord.s_first_name}, {patient_first_name: aRecord.first_name}]);
        }
        //Injection due TODAY (6 months since last injection)
        else if (aRecord.days_since_last == SIX_MONTHS_DAYS && aRecord.on_day_alert_sent == 0)
        {
            console.log(aRecord.days_since_last,SIX_MONTHS_DAYS,aRecord.on_day_alert_sent);

            let id = "on_day";
            aAlert.flag       = id+"_alert_sent";
            aAlert.smsText    = getSMSText("sms_"+id,[{patient_first_name : aRecord.first_name}]);
            aAlert.s_smsText  = aRecord.s_mobile ? getSMSText("sms_s_"+id,[{support_first_name : aRecord.s_first_name}, {patient_first_name: aRecord.first_name}]) : null;
        }
        //OVERDUE: ONE WEEK (6.25 months since last injection)
        else if ( aRecord.days_since_last == ONE_WEEK_OVERDUE && aRecord.six_one_alert_sent == 0)
        {
            let id = "six_one";
            aAlert.flag       = id+"_alert_sent";
            aAlert.smsText    = getSMSText("sms_"+id,[{patient_first_name : aRecord.first_name},{insert_date : moment(aRecord.next_injection_date).format("D MMMM YYYY")}]);
            aAlert.s_smsText  = aRecord.s_mobile ? getSMSText("sms_s_"+id,[{support_first_name : aRecord.s_first_name}, {insert_date : moment(aRecord.next_injection_date).format("D MMMM YYYY")}, {patient_first_name: aRecord.first_name}]) : null;
        }
        //OVERDUE: TWO WEEKS (6.5 months since last injection)
        else if ( aRecord.days_since_last == TWO_WEEKS_OVERDUE && aRecord.six_two_alert_sent == 0)
        {
            setVerificationToken = true;
            let id = "six_two";
            aAlert.flag       = id+"_alert_sent";
            aAlert.emailObj   = getEmail("email_"+id,[{patient_first_name : aRecord.first_name},{insert_link:update_injection_url}]);
            aAlert.s_emailObj = getEmail("email_support_"+id, [{support_first_name: aRecord.s_first_name}, {patient_first_name: aRecord.first_name},{insert_link : s_update_injection_url}]);
            aAlert.smsText    = getSMSText("sms_"+id,[{patient_first_name : aRecord.first_name}]);
            aAlert.s_smsText  = aRecord.s_mobile ? getSMSText("sms_s_"+id,[{support_first_name : aRecord.s_first_name}, {patient_first_name: aRecord.first_name}]) : null;
        }
        //OVERDUE: 1 MONTH 2 WEEKS (7.5 months since last injection)
        else if ( aRecord.days_since_last == SIX_WEEKS_OVERDUE && aRecord.seven_two_alert_sent == 0)
        {
            setVerificationToken = true;
            let id = "seven_two";
            aAlert.flag       = id+"_alert_sent";
            aAlert.emailObj   = getEmail("email_"+id,[{patient_first_name : aRecord.first_name},{insert_link:update_injection_url}]);
            aAlert.s_emailObj = getEmail("email_support_"+id, [{support_first_name: aRecord.s_first_name}, {patient_first_name: aRecord.first_name}, {insert_link : s_update_injection_url}]);
            aAlert.smsText    = getSMSText("sms_"+id,[{patient_first_name : aRecord.first_name},{insert_date : moment(aRecord.next_injection_date).format("D MMMM YYYY")}]);
            aAlert.s_smsText  = aRecord.s_mobile ? getSMSText("sms_s_"+id,[{support_first_name : aRecord.s_first_name}, {insert_date : moment(aRecord.next_injection_date).format("D MMMM YYYY")}, {patient_first_name: aRecord.first_name}]) : null;
        }
        //OVERDUE: 5 MONTHS 2 WEEKS (11.5 months since last injection)
        else if ( aRecord.days_since_last == ELEVEN_HALF_OVERDUE && aRecord.eleven_two_alert_sent == 0)
        {
            setVerificationToken = true;
            let id = "eleven_two";
            aAlert.flag       = id+"_alert_sent";
            aAlert.emailObj   = getEmail("email_"+id,[{patient_first_name : aRecord.first_name},{insert_link:update_injection_url}]);
            aAlert.s_emailObj = getEmail("email_support_"+id, [{support_first_name: aRecord.s_first_name}, {patient_first_name: aRecord.first_name},{insert_link : s_update_injection_url}]);
            aAlert.smsText    = getSMSText("sms_"+id,[{patient_first_name : aRecord.first_name},{insert_date : moment(aRecord.next_injection_date).format("D MMMM YYYY")}]);
            aAlert.s_smsText  = aRecord.s_mobile ? getSMSText("sms_s_"+id,[{support_first_name : aRecord.s_first_name}, {insert_date : moment(aRecord.next_injection_date).format("D MMMM YYYY")}, {patient_first_name: aRecord.first_name}]) : null;
        }

        //If verification token needs setting
        if(setVerificationToken)
        {
            Sql.q("UPDATE Patients SET verification_token = @verification_token, verification_timestamp = GETDATE() WHERE id = @patient_id",
                    {
                        verification_token: verification_token,
                        patient_id : aRecord.patient_id
                    });
        }

        //If an alert has been triggered, then add it
        if(aAlert.flag)
            listOfAlerts.push(aAlert);
    }

    //Foreach alert, send it
    for(var i=0; i<listOfAlerts.length; i++)
    {
        let aAlert = listOfAlerts[i];

        //Set the sent flag to prevent double firing
        let injection_id = aAlert.injection_id;
        Sql.q(`UPDATE Injections SET `+aAlert.flag+`=1 WHERE id=@injection_id`,{injection_id});

        //PATIENT - SMS
        if(aAlert.smsText)
        {
            //let smsText = "mobile: "+aAlert.mobile+"<br><br>"+"Text: "+aAlert.smsText+"<br/><br/>";
            //sendEmail({subject:"SMS Would be sent with text", html:smsText, to:"admin@nathanparrott.com"});
            sendSMS(aAlert.mobile, aAlert.smsText, aAlert.userType, aAlert.userId);
            console.log("Sending PATIENT SMS:"+ aAlert.flag+" for injection_id: "+injection_id);
        }

        //PATIENT - EMAIL
        if(aAlert.emailObj){
            aAlert.emailObj.to = aAlert.email;
            sendEmail(aAlert.emailObj);
            console.log("Sending PATIENT Email:"+ aAlert.flag+" for injection_id: "+injection_id);
        }

        //SUPPORTER - SMS
        if(aAlert.s_smsText){

            aAlert.userType = "Supporters";
            //let smsText = "mobile: "+aAlert.s_mobile+"<br><br>"+"Text: "+aAlert.s_smsText+"<br/><br/>";
            //sendEmail({subject:"SMS Would be sent with text", html:smsText, to:"admin@nathanparrott.com"});
            sendSMS(aAlert.s_mobile, aAlert.s_smsText, aAlert.userType, aAlert.userId);
            console.log("Sending SUPPORTER SMS:"+ aAlert.flag+" for injection_id: "+injection_id);
        }

        //SUPPORTER - EMAIL
        if(aAlert.s_emailObj){
            aAlert.s_emailObj.to = aAlert.s_email;
            sendEmail(aAlert.s_emailObj);
            console.log("Sending SUPPORTER Email:"+ aAlert.flag+" for injection_id: "+injection_id);
        }
    }

  }